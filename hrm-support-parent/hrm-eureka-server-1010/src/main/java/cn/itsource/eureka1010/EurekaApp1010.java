package cn.itsource.eureka1010;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaApp1010 {

    public static void main(String[] args) {
        SpringApplication.run(EurekaApp1010.class);
    }
}
