package cn.itsource.controller;

import cn.itsource.service.ITenantService;
import cn.itsource.hrm.domain.Tenant;
import cn.itsource.hrm.query.TenantQuery;
import cn.itsource.result.JSONResult;
import cn.itsource.result.PageList;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @desc
 * @author å°é«
 * @since 2022-04-26
 */
@RestController
@RequestMapping("/tenant")
public class TenantController {

    @Autowired
    public ITenantService tenantService;

    /**
     * 保存和修改操作公用此方法
     * @param tenant 前端传递来的实体数据
     */
    @PostMapping(value="/save")
    public JSONResult save(@RequestBody Tenant tenant){
        if(tenant.getId()!=null){
                tenantService.updateById(tenant);
        }else{
                tenantService.insert(tenant);
        }
        return JSONResult.success();
    }

    /**
    * 根据ID删除指定对象信息
    * @param id
    */
    @DeleteMapping(value="/{id}")
    public JSONResult delete(@PathVariable("id") Long id){
            tenantService.deleteById(id);
        return JSONResult.success();
    }

    //根据ID查询对象详情信息
    @GetMapping(value = "/{id}")
    public JSONResult get(@PathVariable("id")Long id)
    {
        return JSONResult.success(tenantService.selectById(id));
    }


    /**
    * 查看所有对象数据（不分页）
    */
    @GetMapping(value = "/list")
    public JSONResult list(){
        return JSONResult.success(tenantService.selectList(null));
    }


    /**
    * 分页查询数据
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping(value = "/pagelist")
    public JSONResult pageList(@RequestBody TenantQuery query)
    {
        Page<Tenant> page = new Page<Tenant>(query.getPage(),query.getRows());
        page = tenantService.selectPage(page);
        return JSONResult.success(new PageList<Tenant>(page.getTotal(), page.getRecords()));
    }
}
