package cn.itsource.mapper;

import cn.itsource.hrm.domain.TenantType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 租户(机构)类型表 Mapper 接口
 * </p>
 *
 * @author å°é«
 * @since 2022-04-26
 */
public interface TenantTypeMapper extends BaseMapper<TenantType> {

}
