package cn.itsource.mapper;

import cn.itsource.hrm.domain.Tenant;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author å°é«
 * @since 2022-04-26
 */
public interface TenantMapper extends BaseMapper<Tenant> {

}
