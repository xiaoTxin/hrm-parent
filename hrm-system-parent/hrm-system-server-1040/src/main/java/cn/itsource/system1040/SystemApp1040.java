package cn.itsource.system1040;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SystemApp1040 {

    public static void main(String[] args) {
        SpringApplication.run(SystemApp1040.class);
    }
}
