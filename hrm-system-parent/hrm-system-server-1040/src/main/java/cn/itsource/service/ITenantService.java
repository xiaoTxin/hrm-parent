package cn.itsource.service;

import cn.itsource.hrm.domain.Tenant;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author å°é«
 * @since 2022-04-26
 */
public interface ITenantService extends IService<Tenant> {

}
