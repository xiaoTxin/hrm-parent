package cn.itsource.service;

import cn.itsource.hrm.domain.Department;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author å°é«
 * @since 2022-04-26
 */
public interface IDepartmentService extends IService<Department> {

}
