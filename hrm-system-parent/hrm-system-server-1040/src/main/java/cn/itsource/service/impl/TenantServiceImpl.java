package cn.itsource.service.impl;

import cn.itsource.hrm.domain.Tenant;
import cn.itsource.mapper.TenantMapper;
import cn.itsource.service.ITenantService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author å°é«
 * @since 2022-04-26
 */
@Service
public class TenantServiceImpl extends ServiceImpl<TenantMapper, Tenant> implements ITenantService {

}
