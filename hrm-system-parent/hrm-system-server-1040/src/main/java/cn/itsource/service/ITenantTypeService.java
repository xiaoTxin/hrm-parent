package cn.itsource.service;

import cn.itsource.hrm.domain.TenantType;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 租户(机构)类型表 服务类
 * </p>
 *
 * @author å°é«
 * @since 2022-04-26
 */
public interface ITenantTypeService extends IService<TenantType> {

}
