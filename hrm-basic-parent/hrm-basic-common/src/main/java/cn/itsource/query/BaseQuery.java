package cn.itsource.query;

import lombok.Data;

/**
 * @description:
 * @auth: wujiangbo
 * @date: 2022-01-16 14:08
 */
@Data
public class BaseQuery {

    //关键字
    private String keyword;
    //有公共属性-分页
    private Integer page = 1; //当前页
    private Integer rows = 10; //每页显示多少条
    private Long[] ids; //存放批量操作时，前端传来的数据ID集合
}
